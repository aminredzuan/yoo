from rest_framework import serializers

from django.utils.timezone import now

from .models import (
   Workshop,

)



class WorkshopSerializer(serializers.ModelSerializer):

    class Meta:
        model = Workshop
        fields = '__all__'

