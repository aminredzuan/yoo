from __future__ import unicode_literals 
import uuid 
from django.db import models
from django.utils.formats import get_format
#from django import models
from django.contrib.gis.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


from cars.models import (
    Car
)

from workshops.models import (
    Workshop
)


class Appointment(models.Model):
    
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    appointment_time_date = models.DateTimeField()    
    kedai = models.ForeignKey(Workshop, on_delete=models.CASCADE)
    kereta = models.ForeignKey(Car, on_delete=models.CASCADE)