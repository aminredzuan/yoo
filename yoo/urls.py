
from datetime import datetime, timedelta

from django.conf import settings
from django.conf.urls import include, url
from django.contrib.gis import admin

from rest_framework import routers
from rest_framework_extensions.routers import NestedRouterMixin



from cars.views import (
    CarViewSet,
)



from workshops.views import (
    WorkshopViewSet,
)

from appointments.views import (
    AppointmentViewSet,
)

class NestedDefaultRouter(NestedRouterMixin, routers.DefaultRouter):
    pass



router = NestedDefaultRouter()


cars_router = router.register(
    'cars', CarViewSet
)

workshops_router = router.register(
    'workshops', WorkshopViewSet
)

appointments_router = router.register(
    'appointment', AppointmentViewSet
)




urlpatterns = [

    url(r'v1/', include(router.urls)),

]